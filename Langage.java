package pack;

import static pack.First.*;

public class Langage {



    private void r;

    static int longueur(Liste x) {            // La longueur d'une liste
        if (x == null)
            return 0;
        else
            return 1 + longueur(x.suivant);
    }

    static int kieme(Liste x, int k) {       // Le k ème élément
        if (k == 1)
            return x.val;
        else
            return kieme(x.suivant, k - 1);
    }

    static boolean estDans(String[] x) { // Le test d'appartenance
        if (x == null)
            return false;
        else
            return x.val == v || estDans(x.suivant);
    }

    static boolean accepter(String mot, Automate a) {
        return accepter(mot, a, 0, a.q0);
    }

    static boolean accepter(String mot, Automate a, int i, int q) {
        if (i == mot.length())
            return Liste.estDans(a.finaux, q);
        else {
            boolean resultat = false;
            int c = mot.charAt(i);   // le code ASCII du caractère courant
            for (Liste nv_q = a.delta[q][c]; nv_q != null; nv_q = nv_q.suivant)
                resultat = resultat || accepter(mot, a, i + 1, nv_q.val);
            return resultat;
        }
    }

    public void principale(String[] q) {

        String qo = String.valueOf(new String[Integer.parseInt("S->.S#")]);
        String Q = qo;
        String T = "0";
        while (true) {
            String Qi=Q;
            String Ti=T;
            if (estDans(q)){
                if (estDans((["A->B.Xy,a"]),T){
                    r = Suivants.rechercheSuivant(T);
                    Q.concat("r");
                    T.concat("q,X,r");


                }
            }
            if (Q==Qi && T==Ti) break;



        }
    }


    public String auxiliaire(String q, String p, String b){
        while (true){
            String qi=q;
            for (q:estDans(new String[]{"A->By,a"})){
                for (p:estDans(new String[] {"X->a"})){
                    for (b:estDans(First.recherchePrmier("ya"))) {
                        q.concat("X->.a,b");

                    }

                }

            }
            if (q==qi) break;
        }
        return(q);

    }
    public void DeveloppementBranche(){
         String r="0";
        for (q:estDans(new String[]{"A->B.Xy,a"})) {
            r.concat("A->BX.y,a");
        }
    }
}