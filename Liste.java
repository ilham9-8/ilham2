package pack;

class Liste {
    int val;
    Liste suivant;
    Liste(int v, Liste x) {
        val = v;
        suivant = x;
    }
    static int longueur(Liste x) {            // La longueur d'une liste
        if (x == null)
            return 0;
        else
            return 1 + longueur(x.suivant);
    }
    static int kieme(Liste x, int k) {       // Le k ème élément
        if (k == 1)
            return x.val;
        else
            return kieme(x.suivant, k-1);
    }
    static boolean estDans(Liste x, int v) { // Le test d'appartenance
        if (x == null)
            return false;
        else
            return x.val == v || estDans(x.suivant, v);
    }
}

